<?php

namespace Src\models;

use Src\helpers\Helpers;

class BookingModel {

	private $bookingData;

	private $helper;

	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
	}

	public function getBookings() {
		return $this->bookingData;
	}

	public function createBooking($data) {
		$bookings = $this->getBookings();
		
		$data = $this->validateDiscount($data);
		
		$data['id'] = end($bookings)['id'] + 1;
		$bookings[] = $data;
		$this->helper->putJson($bookings, 'bookings');

		return $data;
	}

	public function validateDiscount($data) {
		$dogsModel = new DogModel();
		$clientDogs = $dogsModel->getDogs();
		$averageAge = 0;
		
		foreach ($clientDogs as $dog) {
			if ($dog['clientid'] == $data['clientid']) {
				if ($averageAge == 0) {
					$averageAge = $dog['age'];
				} else {
					$averageAge = ($averageAge + $dog['age']) / 2;
				}
			}
		}

		if ($averageAge > 0 && $averageAge < 10) {
			$data['price'] = $data['price'] - ($data['price'] * 0.1);
		}

		return $data;
	}

}