<?php

namespace Src\helpers;

class Helpers {

	public static function putJson($users, $entity) {
		file_put_contents(dirname(__DIR__) . '/../scripts/'.$entity.'.json', json_encode($users, JSON_PRETTY_PRINT));
	}
	public static function arraySearchI($needle, $haystack, $column) {
		return array_search($needle, array_column($haystack, $column));
	}
	public function numVerify($phoneNumber, $countryCode = 'US') {
		require_once(__DIR__ . '/vendor/autoload.php');
		$api = new \Numverify\Api('a5059f207fedf0a50f914cd88c004b6c',); // Load key from .env perhaps?
		$validatedPhoneNumber = $api->validatePhoneNumber($phoneNumber, $countryCode);
		return $validatedPhoneNumber->isValid();

		/*
		// Alternative version using cURL
		$numVerify = curl_init('http://apilayer.net/api/validate?access_key='.$access_key.'&number='.$phone_number.'');  
		curl_setopt($numVerify, CURLOPT_RETURNTRANSFER, true);
		$json = curl_exec($numVerify);
		curl_close($numVerify);

		$validatedPhoneNumber = json_decode($json, true);
		return $validatedPhoneNumber['valid'];
		 */
	}
}