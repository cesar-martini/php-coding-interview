<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\helpers\Helpers;

class HelperTest extends TestCase {

	private $helper;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->helper = new Helpers();
	}

	/** @test */
	public function numVerify() {
		$testCorrectPhoneNumber = '4158586273';
		$this->helper->numVerify($testCorrectPhoneNumber);

		$testIncorrectPhoneNumber = '234158586273';
		$this->helper->numVerify($testIncorrectPhoneNumber);

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);
	}
}