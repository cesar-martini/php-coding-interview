<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Dog;

class DogTest extends TestCase {

	private $dog;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->dog = new Dog();
	}

	/** @test */
	public function getDogs() {
		$results = $this->dog->getDogs();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);
		$this->assertEquals(1, 1);
	}
}